using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TaiChiLeaderScript : MonoBehaviour
{

    private Vector3 startPos;   // Starting position
    public float distanceMoved = 15f;
    private NavMeshAgent taiChiLeader;

    void Start()
    {
        taiChiLeader = gameObject.GetComponent<NavMeshAgent>();
        startPos = transform.position;
        StartCoroutine(MoveSequence());
    }

    private IEnumerator MoveSequence()
    {
        // Tai Chi Choreography - forever
        while (true)
        {
            // Move +15 on the z-axis
            yield return StartCoroutine(MoveTo(new Vector3(startPos.x, startPos.y, startPos.z + distanceMoved)));

            // Move +15 on the x-axis
            yield return StartCoroutine(MoveTo(new Vector3(startPos.x + distanceMoved, startPos.y, startPos.z)));

            // Move -15 on the z-axis
            yield return StartCoroutine(MoveTo(new Vector3(startPos.x, startPos.y, startPos.z - distanceMoved)));

            // Move -15 on the x-axis
            yield return StartCoroutine(MoveTo(new Vector3(startPos.x - distanceMoved, startPos.y, startPos.z)));

            // Move back to Start Position
            yield return StartCoroutine(MoveTo(new Vector3(startPos.x, startPos.y, startPos.z)));
        }

    }

    private IEnumerator MoveTo(Vector3 newDestination)
    {
        taiChiLeader.destination = newDestination;

        // We wait until the Tai Chi Leader reaches the new destination
        while (!HasReachedDestination())
        {
            yield return null;
        }
    }

    private bool HasReachedDestination()
    {
        // We return True if it has reached the new destination
        return !taiChiLeader.pathPending && taiChiLeader.remainingDistance <= 1.0f;
    }

}



