// IA_PEC1: Rest state (Idle) script
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RestState : IStateWanderer
{
    private readonly FSMwanderer fsm;   // to access the FSMwanderer
    private readonly Move move;         // to access the Move component

    // We initialize the reference to FSMwanderer to access it from this class
    // We initialize the reference to the Move component (script) to stop/start the agent
    public RestState(FSMwanderer fsmWanderer, Move moveScript)
    {
        fsm = fsmWanderer;
        move = moveScript;
    }

    public void UpdateState()
    {
        // we check if we are already resting, if not we start the resting routine
        if (!move.IsResting)
        {
            move.IsResting = true;
            fsm.StartCoroutine(WaitandMoveAwayFromBench());
        }
    }

    public IEnumerator WaitandMoveAwayFromBench()
    {
        // wait 2 seconds
        yield return new WaitForSeconds(2f);

        // Agent starts moving again, before changing state, so it has some time to move away from the bench
        // before OnTriggerEnter() could stop it again in the WanderingState by transitioning back to RestState
        move.IsResting = false; 
        ToWanderingState();
    }

    public void ToWanderingState()
    { 
        fsm.currentState = fsm.wanderingState;     
    }

    public void ToRestState()
    {
        Debug.Log("Can't change to the same state! Resting...");
    }

    public void OnTriggerEnter(Collider collider)
    {
        // nothing needs to happen here. We are already resting.
    }

}
