// IA_PEC1: movement code for the agents
// Music by Eric Matyas (Royalty-Free, free to use if included author's name)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//---------
// https://docs.unity3d.com/Manual/nav-AgentPatrol.html used as a guideline for this class,

// and for Smoothing using NavMesh {Unity documentation} - calculate a path between two points, and build an array of points in the path
// https://docs.unity3d.com/ScriptReference/AI.NavMeshAgent.CalculatePath.html 
// https://docs.unity3d.com/ScriptReference/AI.NavMeshPath-corners.html
//----------

public class Move : MonoBehaviour
{
    private NavMeshAgent agent;
    public bool IsWanderer = false; // is the agent a wanderer? Or a runner?
    public bool IsResting = false;  // is the wanderer resting? Don't move if it's equal to true.
    public int direction = 1;   // how to move through the waypoints in the array {ascending or descending}
    
    public Transform[] points;  // Waypoints to follow
    private int destPoint = 0;  

    private Vector3[] smoothPathArray;  // Array of smoothing points to follow
    private int smoothPathPosition = 0;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        // Random direction to follow the waypoints array ( +1 or -1, when changing to the next position in the array)
        direction = Random.Range(0, 2) * 2 - 1;

        // Random starting waypoint
        destPoint = Random.Range(0, points.Length);

        // Random speed for the agent depending if it's a runner (faster) or a wanderer (slower)
        if (!IsWanderer)
        {
            agent.speed += Random.Range(-2f, 2f);
        }
        else
        {
            agent.speed += Random.Range(-0.5f, 0.5f);
        }      
    
        agent.autoBraking = false;  // The agent shouldn't slow down as it approaches a destination point
        agent.autoRepath = true;    // The agent calculates a new path if it's interrupted

        GotoNextWayPoint();
    }

    void Update()
    {
        // Choose the next destination point when the agent gets close to the current one AND is not in resting mode (close to a bench).
        if (!agent.pathPending && agent.remainingDistance < 0.5f && !IsResting)
        {
            // Move through the smooth path created with our smoothing algorithm in GotoNextWayPoint()
            if (smoothPathArray != null && smoothPathPosition < smoothPathArray.Length)
            {
                agent.destination = smoothPathArray[smoothPathPosition];
                smoothPathPosition++;
            }
            else  // When smooth path finished, i.e. reached the waypoint, move to the next waypoint
            {
                GotoNextWayPoint();
            }
        }         
    }

    void GotoNextWayPoint()
    {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected waypoint.
        agent.destination = points[destPoint].position;

        // Calculate a smooth path (and stored it in an Array) using a simple smoothing method from NavMesh: CalculatePath, and build the Vector3[] with NavMeshPath.corners
        // Note for CalculatePath: calculate the points in the smooth path {using an overload version of CalculatePath: source is agent's position,
        // and areaMask includes all the NavMesh areas}
        NavMeshPath pathInBetweenWayPoints = new NavMeshPath();
        agent.CalculatePath(agent.destination, pathInBetweenWayPoints); 
        smoothPathArray = pathInBetweenWayPoints.corners;
        smoothPathPosition = 0; // set the position of the first destination point of the smooth path Array

        // Move to the first smooth path destination point
        if (smoothPathArray != null && smoothPathArray.Length > 0)
        {
            agent.destination = smoothPathArray[smoothPathPosition];
        }

        // Prepare the next destination waypoint in the array, for the next time the GotoNextWayPoint() is called,
        // cycling to the start/end if necessary.
        destPoint += direction;
        if (destPoint == points.Length)
        {
            destPoint = 0;
        }
        else if (destPoint < 0)
        {
            destPoint = points.Length - 1;
        }
    }

}