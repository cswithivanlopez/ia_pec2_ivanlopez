// IA_PEC1: FSM Interface script
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// The interface for all the FSM states classes
public interface IStateWanderer
{
    void UpdateState();
    void ToWanderingState();
    void ToRestState();
    void OnTriggerEnter(Collider collider);
}

