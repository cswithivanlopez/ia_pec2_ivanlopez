// IA_PEC1: FSM for the wanderers, script
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FSMwanderer : MonoBehaviour
{
    // Access to the different states
    [HideInInspector] public IStateWanderer currentState;
    [HideInInspector] public WanderingState wanderingState;
    [HideInInspector] public RestState restState;

    // Create the objects for the two possible states
    private void Awake()
    {
        // we pass this FSM to the: WanderingState and RestState objects, for this individual agent
        wanderingState = new WanderingState(this);

        // we also pass access to the Move component so the RestState can stop the agent (and allow it to move again) when needed
        Move moveScript = GetComponent<Move>();
        restState = new RestState(this, moveScript);
    }

    // The start state of the FSM for the agent is to wander.
    void Start()
    {
        currentState = wanderingState;
    }
    
    // We check after every frame if we have to update the state
    void Update()
    {
        currentState.UpdateState(); // this will run either the WanderingState or RestState UpdateState(), depending on the state the FSM is in
    }

    // If we the agent's sphere collider collides with a collider which has a rigid body, it triggers a call
    private void OnTriggerEnter(Collider other)
    {
        currentState.OnTriggerEnter(other); 
    }
}
