// IA_PEC1: Wandering state (Wander) script
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WanderingState : IStateWanderer
{
    private readonly FSMwanderer fsm;       // to access the FSMwanderer
    private bool touchingBench = false;

   
    // We initialize the reference to FSMwanderer to access it from this class
    public WanderingState(FSMwanderer fsmWanderer)
    {
        fsm = fsmWanderer;
    }
    
    public void UpdateState()
    {
        // If it detects a bench nearby it transitions to the RestState
        if (touchingBench) {
            ToRestState(); 
            touchingBench = false;
        }
    }
    // M�todos que ejecutan las transiciones a otros estados
    public void ToWanderingState()
    {
        Debug.Log("Can't change to the same state! Wandering...");
    }

    public void ToRestState()
    {
        fsm.currentState = fsm.restState;
    }

    public void OnTriggerEnter(Collider collider)
    {
        // If it's touching a bench
        if (collider.CompareTag("Bench"))
        {
            touchingBench = true;
        }
        
    }

}
