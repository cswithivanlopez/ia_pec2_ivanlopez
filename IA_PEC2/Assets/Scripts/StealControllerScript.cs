using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealControllerScript : MonoBehaviour
{
    public SoundController soundController; // reference to the sound controller script
    public bool isCaughtStealing = false;       // set when the thief has been caught steeling
    public int chacingTime = 20;  // for how much time will the police office chace the thief
    [Range(0.1f, 1.0f)] public float probabilityOfGettingCaught = 0.5f;  // probability of getting caught 10% -> 100%


    public void Steal()
    {
        // we see if the thief gets caught
        if (Random.value <= probabilityOfGettingCaught)  // returns a float within [0.0, 1.0] If the value is smaller or equal than the probability set above, he gets caught
        {
            // set caught to true and wait 'chacingtime' seconds before the thief stops being chased by the police officer
            isCaughtStealing = true;
            soundController.StartStealingSoundAndPoliceSiren();
            StartCoroutine(CaughtStealingReset());
        }
        else
        {
            soundController.StartStealingSound();
        }
    }

    private IEnumerator CaughtStealingReset()
    {
        // the police officer stops chasing the thief after chasingtime seconds
        yield return new WaitForSeconds(chacingTime);
        isCaughtStealing = false;
        soundController.StopPoliceSiren();
    }
}
